/** @file */

#include "machine.h"

/**
 * @addtogroup Machine
 * @{
 */


/**
 * Initialisiert die Datenfelder des Machine-Objektes.
 *
 * @brief Machine::Machine
 */
Machine::Machine()
{
    for (int i = 0; i < 3; i++){
        m_containers[i] = {
            0,
            false,
            false
        };
    }
    m_transfer = {
        0,
        0,
        false
    };
}

/**
 * Gibt einen Zeiger auf das Struct für einen Behälter zurück.
 *
 * @brief Machine::get_container
 * @param id Nummer des Behälters
 * @return Zeiger auf den Behälter
 */
container_t* Machine::get_container(int id)
{
    if(id < 3)
        return &(m_containers[id]);
    else
        return 0;
}

/**
 * Gibt einen Zeiger auf das Struct für Daten zum Umpumpen zurück.
 *
 * @brief Machine::get_transfer
 * @return Zeiger auf Transfer
 */
transfer_t *Machine::get_transfer()
{
    return &m_transfer;
}

/**
 * Gibt einen Zeiger auf das Struct für Ventile zurück.
 *
 * @brief Machine::get_valve
 * @return Zeiger auf Valves
 */
valve_t *Machine::get_valve()
{
    return &m_valves;
}

/**
 * Gibt einen Zeiger auf das Struct für Pumpen zurück.
 *
 * @brief Machine::get_pump
 * @return Zeiger auf Pumps
 */
pump_t *Machine::get_pump()
{
    return &m_pumps;
}

/** @} */
