/** @file */

#ifndef GUI_H
#define GUI_H

#include <QObject>
#include <QQuaternion>
#include <thread>
#include <chrono>

#include "machine.h"
#include "rvision.h"

class RVision;

/**
 * @defgroup GUI
 * @{
 */

/**
 *
 * @brief Verwaltet die Daten zur Verwendung in QML.
 */
class GUI : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool connected READ connected WRITE setConnected NOTIFY connectedChanged)
    Q_PROPERTY(bool working READ working WRITE setWorking NOTIFY workingChanged)
    Q_PROPERTY(bool fullscreen READ fullscreen WRITE setFullscreen NOTIFY fullscreenChanged)

    Q_PROPERTY(int clock READ clock WRITE setClock NOTIFY clockChanged)

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString version READ version WRITE setVersion NOTIFY versionChanged)

    Q_PROPERTY(double c_level1 READ c_level1 WRITE setC_level1 NOTIFY c_level1Changed)
    Q_PROPERTY(double c_level2 READ c_level2 WRITE setC_level2 NOTIFY c_level2Changed)
    Q_PROPERTY(double c_level3 READ c_level3 WRITE setC_level3 NOTIFY c_level3Changed)

    Q_PROPERTY(double c_last_level1 READ c_last_level1 WRITE setC_last_level1 NOTIFY c_last_level1Changed)
    Q_PROPERTY(double c_last_level2 READ c_last_level2 WRITE setC_last_level2 NOTIFY c_last_level2Changed)
    Q_PROPERTY(double c_last_level3 READ c_last_level3 WRITE setC_last_level3 NOTIFY c_last_level3Changed)

    Q_PROPERTY(int c_sensor1 READ c_sensor1 WRITE setC_sensor1 NOTIFY c_sensor1Changed)
    Q_PROPERTY(int c_sensor2 READ c_sensor2 WRITE setC_sensor2 NOTIFY c_sensor2Changed)
    Q_PROPERTY(int c_sensor3 READ c_sensor3 WRITE setC_sensor3 NOTIFY c_sensor3Changed)

    Q_PROPERTY(int source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(int target READ target WRITE setTarget NOTIFY targetChanged)
    Q_PROPERTY(bool transfer READ transfer WRITE setTransfer NOTIFY transferChanged)

    Q_PROPERTY(bool valve1 READ valve1 WRITE setValve1 NOTIFY valve1Changed)
    Q_PROPERTY(bool valve2 READ valve2 WRITE setValve2 NOTIFY valve2Changed)
    Q_PROPERTY(bool valve3 READ valve3 WRITE setValve3 NOTIFY valve3Changed)
    Q_PROPERTY(bool valve4 READ valve4 WRITE setValve4 NOTIFY valve4Changed)
    Q_PROPERTY(bool valve5 READ valve5 WRITE setValve5 NOTIFY valve5Changed)
    Q_PROPERTY(bool valve6 READ valve6 WRITE setValve6 NOTIFY valve6Changed)
    Q_PROPERTY(bool valve7 READ valve7 WRITE setValve7 NOTIFY valve7Changed)
    Q_PROPERTY(bool valve8 READ valve8 WRITE setValve8 NOTIFY valve8Changed)
    Q_PROPERTY(bool valve9 READ valve9 WRITE setValve9 NOTIFY valve9Changed)

    Q_PROPERTY(bool pump1 READ pump1 WRITE setPump1 NOTIFY pump1Changed)
    Q_PROPERTY(bool pump2 READ pump2 WRITE setPump2 NOTIFY pump2Changed)
    Q_PROPERTY(bool pump3 READ pump3 WRITE setPump3 NOTIFY pump3Changed)

public:
    explicit GUI(QObject *parent = 0);
    ~GUI();
    static void animationClock(GUI*);
    void setBackend(RVision *);
    bool connected();
    void setConnected(bool);
    bool working();
    void setWorking(bool);
    bool fullscreen();
    void setFullscreen(bool);
    double value();
    Q_INVOKABLE void start();
    Q_INVOKABLE void stop();
    Q_INVOKABLE void connect();

    double c_level1();
    void setC_level1(double c_level1);
    double c_level2();
    void setC_level2(double c_level2);
    double c_level3();
    void setC_level3(double c_level3);

    double c_last_level1();
    void setC_last_level1(double c_last_level1);
    double c_last_level2();
    void setC_last_level2(double c_last_level2);
    double c_last_level3();
    void setC_last_level3(double c_last_level3);

    int c_sensor1();
    void setC_sensor1(int c_sensor1);
    int c_sensor2();
    void setC_sensor2(int c_sensor2);
    int c_sensor3();
    void setC_sensor3(int c_sensor3);

    int source();
    void setSource(int source);
    int target();
    void setTarget(int target);
    bool transfer();
    void setTransfer(bool transfer);


    QString name();
    void setName(QString name);

    QString version();
    void setVersion(QString version);

    int clock();
    void setClock(int clock);

    bool valve1();
    void setValve1(bool valve1);
    bool valve2();
    void setValve2(bool valve2);
    bool valve3();
    void setValve3(bool valve3);
    bool valve4();
    void setValve4(bool valve4);
    bool valve5();
    void setValve5(bool valve5);
    bool valve6();
    void setValve6(bool valve6);
    bool valve7();
    void setValve7(bool valve7);
    bool valve8();
    void setValve8(bool valve8);
    bool valve9();
    void setValve9(bool valve9);

    bool pump1();
    void setPump1(bool pump1);
    bool pump2();
    void setPump2(bool pump2);
    bool pump3();
    void setPump3(bool pump3);

Q_SIGNALS:
    void connectedChanged();
    void workingChanged();
    void fullscreenChanged();

    void clockChanged();

    void nameChanged();
    void versionChanged();

    void c_level1Changed();
    void c_level2Changed();
    void c_level3Changed();

    void c_last_level1Changed();
    void c_last_level2Changed();
    void c_last_level3Changed();

    void c_sensor1Changed();
    void c_sensor2Changed();
    void c_sensor3Changed();

    void sourceChanged();
    void targetChanged();
    void transferChanged();

    void valve1Changed();
    void valve2Changed();
    void valve3Changed();
    void valve4Changed();
    void valve5Changed();
    void valve6Changed();
    void valve7Changed();
    void valve8Changed();
    void valve9Changed();

    void pump1Changed();
    void pump2Changed();
    void pump3Changed();
public Q_SLOTS:

private:
    RVision* m_rv;

    bool m_connected;
    bool m_working;
    bool m_fullscreen;

    int m_clock;

    QString m_name;
    QString m_version;

    double m_c_level1;
    double m_c_level2;
    double m_c_level3;

    double m_c_last_level1;
    double m_c_last_level2;
    double m_c_last_level3;

    int m_c_sensor1;
    int m_c_sensor2;
    int m_c_sensor3;

    int m_source;
    int m_target;
    bool m_transfer;

    bool m_valve1;
    bool m_valve2;
    bool m_valve3;
    bool m_valve4;
    bool m_valve5;
    bool m_valve6;
    bool m_valve7;
    bool m_valve8;
    bool m_valve9;

    bool m_pump1;
    bool m_pump2;
    bool m_pump3;

    bool m_clock_active;
    std::thread m_clock_t;
};

/** @} */

#endif // GUI_H
