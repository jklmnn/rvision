/** @file */

#ifndef RVISION_H
#define RVISION_H

#include <thread>
#include <chrono>
#include <string>
#include <condition_variable>
#include <mutex>

#include "gui.h"
#include "opcua.h"
#include "logging.h"
#include "machine.h"

class OPCUA;
class GUI;

/**
 * @defgroup RVision
 * @{
 */

/**
 *
 * @brief Hauptklasse des Programms, dient zur Verknüpfung der anderen Komponenten.
 *
 */
class RVision
{
public:
    RVision(GUI*, std::string, LogLevel);
    static void start_opc_thread(RVision*);
    static void callback_connected(RVision*, int);
    static void callback_working(RVision*, bool);
    static void callback_update(RVision*);
    void connect();
    void start();
    void pause();
    ~RVision();
private:
    void connected(int);
    void working(bool);
    void update();
    GUI *m_gui;
    OPCUA *m_opc;
    std::thread m_opc_thread;
    Logging *m_logger;
    bool m_active, m_paused, m_connected, m_connecting;
    Machine *m_machine;
    std::condition_variable m_cv;
    std::mutex m_mutex;
};

/** @} */

#endif // RVISION_H
