/** @file */

/**
 * @addtogroup OPCUA
 * @{
 */

#include "opcua.h"

/**
 * Initialisiert die Instanz von OPCUA. Dabei werden die Klassenvariablen initialisiert, die Pythonruntime gestartet und das opcua-Modul von Python geladen.
 *
 * @brief OPCUA::OPCUA
 * @param url OPC UA Url
 * @param loglevel LogLevel
 */
OPCUA::OPCUA(std::string url, LogLevel loglevel)
{
    m_url = url;
    m_p_data_node = 0;
    m_p_client = 0;
    m_connected = false;
    m_logger = new Logging(loglevel);

    Py_Initialize();
    PyObject *p_module_name = PyUnicode_DecodeFSDefault("opcua");
        PyObject *p_opcua = PyImport_Import(p_module_name);
        if (p_opcua){
            Py_DECREF(p_module_name);
            const std::string string_array[1] = { m_url };
            p_call_opc(p_opcua, std::string("Client"), string_array, 1, &m_p_client);
            if (!m_p_client)
                m_logger->error("Unable to get opcua client object.\n");
            Py_DECREF(p_opcua);
        }else{
            if(PyErr_Occurred())
                PyErr_Print();
            m_logger->error("Couldn't load module opcua.\n");
      }
}

/**
 * Verbindet zum OPCUA Server und lädt die benötigten Wurzelknoten. Bei Erfolg oder Abbruch wird RVision darüber informiert.
 *
 * @brief OPCUA::connect
 * @param rv_cb Callbackinstanz
 */
void OPCUA::connect(RVision* rv_cb)
{
    if (m_p_client){
        PyObject *dummy = 0, *p_object_node = 0;
        p_call_opc(m_p_client, "connect", {}, 0, &dummy);
        p_call_opc(m_p_client, "get_objects_node", 0, 0, &p_object_node);
        if (p_object_node){
            PyObject *p_server;
            const std::string xmlda[1] = { XMLDASERVER };
            p_call_opc(p_object_node, "get_child", xmlda, 1, &p_server);
            if (p_server){
                const std::string schneider[1] = { SCHNEIDER };
                p_call_opc(p_server, "get_child", schneider, 1, &m_p_data_node);
                Py_DECREF(p_server);
            }else{
                fprintf(stderr, "Connection error\n");
                if(PyErr_Occurred())
                    PyErr_Print();
                RVision::callback_connected(rv_cb, 1);
                return;
            }
        }else{
            fprintf(stderr, "Invalid object node\n");
            if(PyErr_Occurred())
                PyErr_Print();
            RVision::callback_connected(rv_cb, 1);
            return;
        }
        m_connected = true;
        RVision::callback_connected(rv_cb, 0);
    }else{
        fprintf(stderr, "Invalid client object.\n");
        if(PyErr_Occurred())
            PyErr_Print();
        RVision::callback_connected(rv_cb, 1);
    }
}

/**
 * Liest den Füllstand eines Behälters aus.
 *
 * @brief OPCUA::get_level
 * @param id Nummer des Behälters
 * @param rv Callbackinstanz
 * @param level Füllstand
 * @return Fehlercode
 */
int OPCUA::get_level(int id, RVision *rv, double *level)
{
    PyObject *p_value = 0;
    std::string identifier = FUELLSTAND + std::to_string(id) + IST;
    int code = get_value(identifier, &p_value, rv);
    if(code){
        if (p_value)
            Py_DECREF(p_value);
        return code;
    }
    if(p_value){
        *level = PyFloat_AsDouble(p_value);
        Py_DECREF(p_value);
        return 0;
    }else{
        return 1;
    }
}

/**
 * Liest Ziel- und Quellbehälter des Umpumpvorganges aus.
 *
 * @brief OPCUA::get_container
 * @param identifier OPC UA Identifier
 * @param rv Callbackinstanz
 * @param container Nummer des Behälters
 * @return Fehlercode
 */
int OPCUA::get_container(std::string identifier, RVision *rv, int *container)
{
    PyObject *p_value = 0;
    int code = get_value(identifier, &p_value, rv);
    if(code){
        if(p_value)
            Py_DECREF(p_value);
        if(PyErr_Occurred())
            PyErr_Print();
        return code;
    }
    if(p_value){
        *container = (int)PyLong_AsLong(p_value);
        Py_DECREF(p_value);
        return 0;
    }else{
        return 1;
    }
}

/**
 * Liest die Aktivität einer Pumpe oder eines Ventils aus. Für nichtboolsche Werte, etwa von P3, wird 0 implizit als False und alles andere implizit als True interpretiert.
 *
 * @brief OPCUA::get_active
 * @param identifier OPC UA Identifier
 * @param rv Callbackinstanz
 * @param active Status der Pumpe/des Ventils
 * @return Fehlercode
 */
int OPCUA::get_active(std::string identifier, RVision *rv, bool *active)
{
    PyObject *p_value;
    int code = get_value(identifier, &p_value, rv);
    if (code){
        if(p_value)
            Py_DECREF(p_value);
        if(PyErr_Occurred())
            PyErr_Print();
        return code;
    }
    if(p_value){
        *active = PyObject_IsTrue(p_value);
        Py_DECREF(p_value);
        return 0;
    }else{
        return 1;
    }
}

/**
 * Liest einen Sensorwert als bool aus.
 *
 * @brief OPCUA::get_sensor
 * @param sensor Oberer/Unterer Sensor
 * @param id Nummer des Behälters
 * @param rv Callbackinstanz
 * @param triggered Status des Sensors
 * @return Fehlercode
 */
int OPCUA::get_sensor(std::string sensor, int id, RVision *rv, bool *triggered)
{
    PyObject *p_value = 0;
    std::string identifier = sensor + std::to_string(id);
    int code = get_value(identifier, &p_value, rv);
    if(code){
        if (p_value)
            Py_DECREF(p_value);
        if(PyErr_Occurred())
            PyErr_Print();
        return code;
    }
    if(p_value){
        *triggered = PyObject_IsTrue(p_value);
        Py_DECREF(p_value);
        return 0;
    }else{
        if(PyErr_Occurred())
            PyErr_Print();
        return 1;
    }
}

/**
 * Aktualisiert die Datenklasse Machine und informiert RVision über den Status der Aktualisierung. Gibt einen Fehlercode zurück.
 *
 * @brief OPCUA::update
 * @param machine Instanz des Datenobjekts
 * @param rv Callbackinstanz
 * @return Fehlercode
 */
int OPCUA::update(Machine *machine, RVision *rv)
{
    pump_t *pumps = machine->get_pump();
    valve_t *valves = machine->get_valve();
    transfer_t *transfer = machine->get_transfer();
    for (int i = 0; i < 3; i++){
        container_t *cont = machine->get_container(i);
        if(get_level(i + 1, rv, &(cont->level)))
            { m_connected = false; return 1; }
        if(get_sensor(SENS_LOW, i + 1, rv, &(cont->lower)))
            { m_connected = false; return 1; }
        if(get_sensor(SENS_HIGH, i + 1, rv, &(cont->upper)))
            { m_connected = false; return 1; }
    }

    if(get_container(TRANS_SRC, rv, &(transfer->source)))
        { m_connected = false; return 1; }
    if(get_container(TRANS_TGT, rv, &(transfer->target)))
        { m_connected = false; return 1; }
    if(get_active(TRANSFER, rv, &(transfer->active)))
        { m_connected = false; return 1; }

    for(int i = 0; i < 7; i++){
        if(get_active(VALVE_MAG + std::to_string(i + 1), rv, &(valves->state[i])))
            { m_connected = false; return 1; }
    }
    for(int i = 0; i < 2; i++){
        if(get_active(VALVE_VEN + std::to_string(i + 1), rv, &(valves->state[i + 7])))
            { m_connected = false; return 1; }
    }
    for(int i = 0; i < 3; i++){
        if(get_active(PUMP + std::to_string(i + 1), rv, &(pumps->state[i])))
            { m_connected = false; return 1; }
    }

    RVision::callback_update(rv);
    return 0;
}

/**
 * Beendet die Instanz. Trennt die Verbindung zum Server, dekrementiert die Referenzzähler der Verbliebenen Pythonobjekte und finalisiert die Pythonruntime.
 *
 * @brief OPCUA::~OPCUA
 */
OPCUA::~OPCUA()
{
    if(m_p_data_node && m_p_client){
        PyObject *dummy = 0;
        m_logger->debug("Disconnecting..");
        if(m_connected){
            p_call_opc(m_p_client, "disconnect", {}, 0, &dummy);
            Py_DECREF(m_p_data_node);
            Py_DECREF(m_p_client);
            Py_DECREF(dummy);
        }
    }
    m_logger->debug("Finalizing..");
    Py_Finalize();
}

/**
 * Generischer Wrapper zum Aufruf der OPC UA Funktionen in Python.
 *
 * @brief OPCUA::p_call_opc
 * @param parent Caller der Funktion
 * @param func Funktionsname
 * @param args Liste von Parametern
 * @param size Anzahl der Parameter
 * @param result Rückgabe der Funktion
 * @return Fehlercode
 */
int OPCUA::p_call_opc(PyObject *parent, const std::string func, const std::string args[], const size_t size, PyObject **result)
{
    PyObject *p_args;
    if (size)
        p_args = PyTuple_New(size);
    PyObject *p_value = 0;
    for(size_t i = 0; i < size; i++){
        p_value = PyUnicode_DecodeFSDefault(args[i].c_str());
        PyTuple_SetItem(p_args, i, p_value);
    }
    PyObject *p_func;
    if (parent)
        p_func = PyObject_GetAttrString(parent, func.c_str());
    else
        m_logger->error("Could not find function.\n");

    if (p_func){
        if(size){
            *result = PyObject_CallObject(p_func, p_args);
        }else{
            *result = PyObject_CallObject(p_func, 0);
        }
    }else{
        m_logger->error("Invalid function object\n");
    }

    if (!*result){
        if(PyErr_Occurred())
            PyErr_Print();
    }
    if (size)
        Py_DECREF(p_args);
    if (p_func)
        Py_DECREF(p_func);
    return 0;
}

/**
 * Generischwer Wrapper zum Abruf eines Wertes vom Server.
 *
 * @brief OPCUA::get_value
 * @param identifier OPC UA Identifier
 * @param p_value Rückgabe der Funktion
 * @param work_notifier Callbackinstanz
 * @return Fehlercode
 */
int OPCUA::get_value(std::string identifier, PyObject **p_value, RVision *work_notifier)
{
    if (work_notifier)
        RVision::callback_working(work_notifier, true);
    int code;
    if (m_p_data_node){
        PyObject *p_var = 0;
        std::string arg[1] = { identifier };
        p_call_opc(m_p_data_node, "get_child", arg, 1, &p_var);
        if (p_var){
            p_call_opc(p_var, "get_value", {}, 0, p_value);
            Py_DECREF(p_var);
            if (p_value){
                code = 0;
            }else{
                m_logger->error("Could not access value.");
                if(PyErr_Occurred())
                    PyErr_Print();
                code = 1;
            }
        }else{
            m_logger->error("Could not access variable.");
            if(PyErr_Occurred())
                PyErr_Print();
            code = 1;
        }
    }else{
        m_logger->error("Connection lost.");
        if(PyErr_Occurred())
            PyErr_Print();
        code = 1;
    }
    if (work_notifier)
        RVision::callback_working(work_notifier, false);
    return code;
}

/** @} */
