/** @file */

#ifndef LOGGING_H
#define LOGGING_H

#include <string>
#include <iostream>

/**
 * @defgroup Logging
 * @{
 */

/**
 * Gibt gar keine Logs aus.
 */
#define LL_NONE 0
/**
 * Gibt lediglich Fehler aus.
 */
#define LL_ERROR 1
/**
 * Gibt Fehler und Informationen aus. Sollte als Standard verwendet werden.
 */
#define LL_INFO 2
/**
 * Gibt Debugmeldungen, Fehler und Informationen aus. Sollte nur zum Debuggen verwendet werden.
 */
#define LL_DEBUG 3
/**
 * Gibt alle Meldungen aus, nicht weiter Implementiert.
 */
#define LL_ALL 4

typedef int LogLevel;

/**
 * @brief Klasse zum Ausgeben von Logs. Es kann ein Loglevel angegeben werden sodass auch reduzierte oder erweiterte Meldungen ausgegeben werden können.
 */
class Logging
{
public:
    Logging(LogLevel);
    void error(std::string);
    void info(std::string);
    void debug(std::string);
private:
    LogLevel m_loglevel;
};

/** @} */
#endif // LOGGING_H
