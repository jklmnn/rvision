/** @file */

#include "logging.h"

/**
 * @addtogroup Logging
 * @{
 */

/**
 * Initialisiert das zu nutzende Level der Ausgaben.
 *
 * @brief Logging::Logging
 * @param ll LogLevel
 */
Logging::Logging(LogLevel ll)
{
    m_loglevel = ll;
}

/**
 * Gibt Meldungen ab dem Level LL_DEBUG und höher aus.
 *
 * @brief Logging::debug
 * @param message Logmeldung
 */
void Logging::debug(std::string message)
{
    if (m_loglevel >= LL_DEBUG)
        std::cout << message << std::endl;
}

/**
 * Gibt Meldungen ab dem Level LL_INFO und höher aus.
 *
 * @brief Logging::info
 * @param message Logmeldung
 */
void Logging::info(std::string message)
{
    if (m_loglevel >= LL_INFO)
        std::cout << message << std::endl;
}

/**
 * Gibt Meldungen ab dem Level LL_ERROR und höher aus. Alle Meldungen dieser Funktionen werden nach stderr statt stdout geschrieben.
 *
 * @brief Logging::error
 * @param message Logmeldung
 */
void Logging::error(std::string message)
{
    if (m_loglevel >= LL_ERROR){
        std::cerr << message << std::endl;
        if(m_loglevel > LL_ERROR)
            std::cout << std::endl;
    }
}

/** @} */
