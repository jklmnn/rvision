/** @file */

#include "rvision.h"

/**
 * @addtogroup RVision
 * @{
 */

/**
 * Initialisiert alle Membervariablen mit sinnvollen Standardwerten. Erzeugt außerdem ein OPCUA-Objekt sowie ein eigenes Logger-Objekt.
 *
 * @brief RVision::RVision
 * @param gui Instanz der GUI
 * @param url OPC UA Url
 * @param loglevel LogLevel
 */
RVision::RVision(GUI *gui, std::string url, LogLevel loglevel)
{
    m_gui = gui;
    gui->setBackend(this);
    m_opc = new OPCUA(url, loglevel);
    m_logger = new Logging(loglevel);
    m_active = false;
    m_machine = new Machine();
    m_active = true;
    m_paused = true;
    m_connected = false;
    m_connecting = false;
}

/**
 * Startet den Thread zum Verbinden mit dem OPC UA Server sowie zum aktualisieren der Daten.
 *
 * @brief RVision::start_opc_thread
 * @param self Referenz auf die startende Instanz
 */
void RVision::start_opc_thread(RVision *self)
{
    self->working(true);
    self->m_connecting = true;
    self->m_logger->info("Connecting...");
    self->m_opc->connect(self);
    while (self->m_active && self->m_connected){
        if (self->m_opc->update(self->m_machine, self)){
            self->m_connected = false;
            self->m_gui->setConnected(false);
        }
        std::this_thread::sleep_for(std::chrono::seconds(1));
        if (self->m_paused){
            std::unique_lock<std::mutex> lock(self->m_mutex);
            self->m_cv.wait(lock, [self]{return !(self->m_paused);});
            self->m_logger->debug("unlocking");
            lock.unlock();
            self->m_logger->debug("unlocked");
        }
    }
}

/**
 * Callback zur Signalisierung des Verbindungsaufbaus.
 *
 * @brief RVision::callback_connected
 * @param self Zielinstanz des Callbacks
 * @param code Fehlercode
 */
void RVision::callback_connected(RVision *self, int code)
{
    self->connected(code);
}


/**
 * Callback der mitteilt, ob die Anwendung beschäftigt ist.
 *
 * @brief RVision::callback_working
 * @param self Zielinstanz des Callbacks
 * @param status Arbeitsstatus
 */
void RVision::callback_working(RVision *self, bool status)
{
    self->working(status);
}


/**
 * Callback der signalisiert, dass das Update beendet ist und die GUI aktualisiert werden kann.
 *
 * @brief RVision::callback_update
 * @param self Zielinstanz des Callbacks
 */
void RVision::callback_update(RVision *self)
{
        self->update();
}


/**
 * Startet einen Verbindungsversuch.
 *
 * @brief RVision::connect
 */
void RVision::connect()
{
    if(!(m_connected || m_connecting)){
        if(m_opc_thread.joinable())
            m_opc_thread.join();
        m_opc_thread = std::thread(start_opc_thread, this);
    }
}


/**
 * Startet den Netzwerkthread.
 *
 * @brief RVision::start
 */
void RVision::start()
{
    m_paused = false;
    m_cv.notify_all();
}

/**
 * Pausiert den Netzwerkthread.
 *
 * @brief RVision::pause
 */
void RVision::pause()
{
    m_paused = true;
}

/**
 * Beendet sauber den Netzwerkthread und den OPC UA Client.
 *
 * @brief RVision::~RVision
 */
RVision::~RVision()
{
    m_logger->info("Exiting");
    start();
    m_active = false;
    m_opc_thread.join();
    delete m_opc;
}

/**
 * Setzt den Verbindungsstatus abhängig vom gegebenen Fehlercode.
 *
 * @brief RVision::connected
 * @param error Fehlercode
 */
void RVision::connected(int error)
{
    if(!error){
        m_gui->setConnected(true);
        m_logger->info("Connected.");
        m_connected = true;
    }else{
        m_gui->setConnected(false);
        m_logger->error("Connection failure.");
        m_connected = false;
    }
    working(false);
    m_connecting = false;
}

/**
 * Setzt den Arbeitsstatus.
 *
 * @brief RVision::working
 * @param status Arbeitsstatus
 */
void RVision::working(bool status)
{
    m_gui->setWorking(status);
}


/**
 * Aktualisiert sämtliche Werte der GUI.
 *
 * @brief RVision::update
 */
void RVision::update()
{
    m_gui->setC_level1(m_machine->get_container(0)->level);
    m_gui->setC_level2(m_machine->get_container(1)->level);
    m_gui->setC_level3(m_machine->get_container(2)->level);

    int sensors[3];
    for (int i = 0; i < 3; i++){
        if(!m_machine->get_container(i)->lower)
            sensors[i] = -1;
        else if(m_machine->get_container(i)->upper)
            sensors[i] = 1;
        else
            sensors[i] = 0;
    }

    m_gui->setC_sensor1(sensors[0]);
    m_gui->setC_sensor2(sensors[1]);
    m_gui->setC_sensor3(sensors[2]);

    m_gui->setSource(m_machine->get_transfer()->source);
    m_gui->setTarget(m_machine->get_transfer()->target);
    m_gui->setTransfer(m_machine->get_transfer()->active);

    m_gui->setPump1(m_machine->get_pump()->state[0]);
    m_gui->setPump2(m_machine->get_pump()->state[1]);
    m_gui->setPump3(m_machine->get_pump()->state[2]);

    m_gui->setValve1(m_machine->get_valve()->state[0]);
    m_gui->setValve2(m_machine->get_valve()->state[1]);
    m_gui->setValve3(m_machine->get_valve()->state[2]);
    m_gui->setValve4(m_machine->get_valve()->state[3]);
    m_gui->setValve5(m_machine->get_valve()->state[4]);
    m_gui->setValve6(m_machine->get_valve()->state[5]);
    m_gui->setValve7(m_machine->get_valve()->state[6]);
    m_gui->setValve8(m_machine->get_valve()->state[7]);
    m_gui->setValve9(m_machine->get_valve()->state[8]);
}

/** @} */
