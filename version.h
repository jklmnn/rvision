/** @file */

#ifndef VERSION_H
#define VERSION_H

/**
 * @defgroup Version
 *
 * Verwaltet Programmname und Version
 *
 * @{
 */

/**
 * Definition des Programmnamens
 */
#define RV_NAME "RVision"
/**
 * Definition der Programmversion
 */
#define RV_VERSION "0.3.0"

/** @} */

#endif // VERSION_H
