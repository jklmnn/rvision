#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QSettings>
#include <QCommandLineParser>
#include "gui.h"
#include "logging.h"
#include "rvision.h"
#include "version.h"

/** @mainpage
 * @tableofcontents
 * @section setup Einrichtung
 *
 * Zum Kompilieren werden die folgenden Pakete in Debian benötigt:
 *   - libpython3-dev
 *   - qt5-default
 *   - qt5-qmake
 *   - qtdeclarative5-dev
 *
 * Die Kompilierung erfolgt mittels `qmake && make` im Projektordner oder alternativ über den Import in QtCreator.
 *
 * Zur Ausführung muss zusätzlich noch das Pythonpaket freeopcua mit pip3 installiert werden.
 *
 * @section usage Nutzung
 *
 * Die Konfiguration erfolgt über Kommandozeilenparameter und eine Konfigurationsdatei.
 * Es sind folgende Argumente verfügbar:
 *
 *   - `-h`, `--help`: Ausgabe der Hilfe.
 *   - `-v`, `--version`: Angabe der Version.
 *   - `-u`, `--url`: Angabe der OPC UA Url.
 *   - `-f`, `--fullscreen`: Starten des Programms im Vollbildmodus.
 *   - `-q`, `--quiet`: Setzen der Logausgaben auf `LL_ERROR`.
 *   - `-l`, `--loud`: Setzen der Logausgaben auf `LL_DEBUG`.
 *   - `-s`, `--save`: Speichern der Konfiguration.
 *
 * Beim Speichern ist zu beachten, dass nicht gesetzte Parameter auch gelöscht werden. Wird `-s` verwendet, sollten alle Parameter neu gesetzt werden, auch wenn diese sich nicht ändern.
 * Die Konfiguration wird unter `$HOME/.config/EATS/RVision.conf` gespeichtert.
 * Die Parameter `-q` und `-l` dürfen nicht zusammen genutzt werden.
 *
 * @section libraries Externe Bibliotheken
 *
 * Es wurden die Bibiliotheken Qt, Python und FreeOpcUa genutzt. ES wird hierbei auf deren eigene Dokumentation verwiesen.
 *
 *   - Qt: https://doc.qt.io/
 *   - Python3: https://docs.python.org/3/c-api/
 *   - FreeOpcUa: https://github.com/FreeOpcUa/freeopcua#usage
 *
 * Anmerkung: Da sowohl Python als auch Qt das Schlüsselwort `signal` verwenden, wird das Projekt mit der Option `no_keywords` kompiliert. Zudem werden als Ersatz in Qt Makros verwendet.
 *
 * @section raspberrypi Raspberry Pi
 *
 * Der Vollständigkeit halber wird hier die Konfiguration des Raspberry Pis mit aufgeführt.
 *
 * @subsection gui Oberfläche
 *
 * Das Raspberry startet direkt in das Programm. Dabei kommt der Displaymanager `lightdm` zum Einsatz.
 * Dieser ist so konfiguriert, dass er eine Anmeldung automatisch vornimmt.
 * Außerdem ist der Mauszeiger standardmäßig deaktiviert und erscheint nur beim Klicken oder Bewegen der Maus.
 * Als Fenstermanager kommt `Openbox` zum Einsatz. Es werden keine zusätzlichen Statusleisten angezeigt.
 * Die Datei `$HOME/.config/openbox/autostart` startet den Pfad `$HOME/bin/rvision`.
 * An dieser Stelle liegt ein symbolischer Link auf `$HOME/rvision/rvision`, was die ausführbare Datei darstellt.
 *
 * @subsection network Netzwerk
 *
 * Das Netzwerk ist ohne weitere Einrichtung verblieben.
 * `eth0` akzeptiert DHCP, ein weiteres Interface wurde nicht eingesetzt.
 * Das Raspberry muss daher dem Zielnetzwerk entsprechend eingerichtet werden.
 *
 */


/**
 * Initialisieren des Programms. Die Kommandozeilenparameter werden mit QCommandLineParser geparst und die Einstellungen mit QSettings gespeichert. Es wird auch der Logger eingestellt und die GUI- und QML-Komponenten verknüpft.
 *
 * @brief main
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QGuiApplication::setApplicationName(RV_NAME);
    QGuiApplication::setApplicationVersion(RV_VERSION);

    QSettings settings("EATS", RV_NAME);
    settings.beginGroup("default");

    QCommandLineParser parser;
    parser.setApplicationDescription("Raspberry Pi Machine Visualization");
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption co_url(QStringList() << "u" << "url",
                                QGuiApplication::translate("main", "Url for binary OPC"),
                                QGuiApplication::translate("main", "url"));
    parser.addOption(co_url);
    QCommandLineOption co_save(QStringList() << "s" << "save",
                                QGuiApplication::translate("main", "Save current options"));
    parser.addOption(co_save);
    QCommandLineOption co_fullscreen(QStringList() << "f" << "fullscreen",
                                QGuiApplication::translate("main", "Start in fullscreenmode."));
    parser.addOption(co_fullscreen);
    QCommandLineOption co_verbose(QStringList() << "l" << "loud",
                                QGuiApplication::translate("main", "Verbose output, collides with --quiet."));
    parser.addOption(co_verbose);
    QCommandLineOption co_quiet(QStringList() << "q" << "quiet",
                                QGuiApplication::translate("main", "Less output, collides with --verbose."));
    parser.addOption(co_quiet);

    parser.process(app);
    QString url = parser.value(co_url);
    bool fullscreen = parser.isSet(co_fullscreen);
    LogLevel loglevel = LL_INFO;


    if (parser.isSet(co_save)){
        if (parser.isSet(co_fullscreen))
            settings.setValue("fullscreen", fullscreen);
        if (parser.isSet(co_url))
            settings.setValue("url", url);
    }

    if(parser.isSet(co_verbose) && parser.isSet(co_quiet)){
        return 2;
    }else if(parser.isSet(co_verbose)){
        loglevel = LL_DEBUG;
        if (parser.isSet(co_save))
            settings.setValue("loglevel", loglevel);
    }else if(parser.isSet(co_quiet)){
        loglevel = LL_ERROR;
        if (parser.isSet(co_save))
            settings.setValue("loglevel", loglevel);
    }else{
        loglevel = settings.value("loglevel").toInt();
    }

    Logging *l = new Logging(loglevel);

    if (url.isEmpty())
        url = settings.value("url", "").toString();
    if (url.isEmpty()){
        l->error("No url specified, exiting.");
        return 1;
    }

    l->info("Starting...");
    GUI *gui = new GUI();
    gui->setName(RV_NAME);
    gui->setVersion(RV_VERSION);

    if (fullscreen || settings.value("fullscreen", false).toBool())
        gui->setFullscreen(true);

    settings.endGroup();
    RVision *rvapp = new RVision(gui, url.toStdString(), loglevel);
    rvapp->connect();

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty(QStringLiteral("GUI"), gui);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    int code = app.exec();

    delete rvapp;
    l->info("Exit.");
    return code;
}
