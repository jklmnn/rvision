TEMPLATE = app

QT += qml quick
CONFIG += c++11

SOURCES += main.cpp \
    gui.cpp \
    opcua.cpp \
    logging.cpp \
    rvision.cpp \
    machine.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    gui.h \
    opcua.h \
    logging.h \
    rvision.h \
    opcua_constants.h \
    machine.h \
    version.h

unix: CONFIG += link_pkgconfig no_keywords
unix: PKGCONFIG += python3
