import QtQuick 2.3
import QtQuick.Window 2.2

Window {
    id: main_window
    visible: true
    width: 320
    height: 240
    title: GUI.name
    color: palette.lightBackground
    visibility: GUI.fullscreen ? Window.FullScreen : Window.Windowed;

    Item {
        id: palette
        property color darkAccent: "#475057"
        property color darkBackground: "#556068"

        property color mediumBackground: "#7f8c8d"

        property color lightAccent: "#eff0f1"
        property color lightBackground: "#fcfcfc"


        property color good: "#27ae60"
        property color medium: "#f67400"
        property color bad: "#da4453"
        property color neutral: "#3daee9"
    }

    Item {
        id:constants

        property int max_fill: 279

        property int pipe_size: 5
        property int pipe_conn_size: 9
        property int valve_size: 11
        property int pump_size: 11
    }

    Rectangle {
        id: topbar
        width: main_window.width
        height: 30
        x: 0
        y: 0
        //color: palette.darkBackground
        gradient: Gradient {
            GradientStop {position: 0; color: palette.darkBackground }
            GradientStop {position: 1; color: palette.darkAccent }
        }

        Rectangle {
            id: menu_button
            width: 24
            height: 24
            color: "transparent"
            border.width: 2
            border.color: palette.lightBackground
            radius: 4
            anchors.left: topbar.left
            anchors.leftMargin: 10
            anchors.verticalCenter: topbar.verticalCenter

            MouseArea {
                width: menu_button.width
                height: menu_button.height
                onClicked: {
                    view_simple.visible = false
                    view_tabbed.visible = false
                    view_graph.visible = false
                    menu.visible = true
                    if (!GUI.menu){
                        GUI.stop();
                    }
                }
                onPressed: {
                    menu_button.color = palette.lightBackground
                    mbar1.color = palette.darkBackground
                    mbar2.color = palette.darkBackground
                }
                onReleased: {
                    menu_button.color = palette.darkBackground
                    mbar1.color = palette.lightBackground
                    mbar2.color = palette.lightBackground
                }
            }

            Rectangle {
                id: mbar1
                width: 16
                height: 4
                anchors.top: menu_button.top
                anchors.topMargin: 6
                anchors.horizontalCenter: menu_button.horizontalCenter
                radius: 2
                color: palette.lightAccent
            }

            Rectangle {
                id: mbar2
                width: 16
                height: 4
                anchors.top: mbar1.bottom
                anchors.topMargin: 4
                anchors.horizontalCenter: menu_button.horizontalCenter
                radius: 2
                color: palette.lightAccent
            }
        }

        Text {
            text: GUI.name + " " + GUI.version
            font.capitalization: Font.AllUppercase
            font.pointSize: 14
            color: palette.lightBackground
            anchors.verticalCenter: topbar.verticalCenter
            anchors.left: menu_button.right
            anchors.leftMargin: 10
        }

        Rectangle {
            id: conn_indicator_box
            width: 16
            height: 16
            color: "transparent"
            Rectangle {
                id: conn_indicator
                width: 16 - (GUI.clock * (GUI.working ? 6 : 0)) % 8
                height: 16 - (GUI.clock * (GUI.working ? 6 : 0)) % 8
                color: GUI.connected ? palette.good : palette.bad;
                radius: 16
                anchors.centerIn: parent
            }
            anchors.verticalCenter: topbar.verticalCenter
            anchors.right: topbar.right
            anchors.rightMargin: 10
        }
    }

    Rectangle {
        id: menu
        visible: true
        width: main_window.width
        height: main_window.height - topbar.height
        color: palette.darkBackground
        z: 1
        anchors.top: topbar.bottom
        anchors.left: topbar.left

        Rectangle {
            id: start_simple
            width: 80
            height: 80
            color: palette.darkBackground
            border.width: 2
            border.color: GUI.connected ? palette.lightBackground : palette.mediumBackground
            radius: 4
            anchors.bottom: menu.verticalCenter
            anchors.bottomMargin: 5
            anchors.right: start_tabbed.left
            anchors.rightMargin: 10

            MouseArea {
                anchors.centerIn: parent
                width: parent.width
                height: parent.height
                onClicked: {
                    view_simple.visible = true
                    menu.visible = false
                    GUI.start();
                }
                onPressed: {
                    start_simple.color = palette.lightBackground
                    start_simple_text.color = palette.darkBackground
                }
                onReleased: {
                    start_simple.color = palette.darkBackground
                    start_simple_text.color = GUI.connected ? palette.lightBackground : palette.mediumBackground
                }
                enabled: GUI.connected
            }

            Text {
                id: start_simple_text
                color: GUI.connected ? palette.lightBackground : palette.mediumBackground
                anchors.centerIn: parent
                text: "simple"
                font.capitalization: Font.AllUppercase
            }
        }

        Rectangle {
            id: start_tabbed
            width: 80
            height: 80
            color: palette.darkBackground
            border.width: 2
            border.color: GUI.connected ? palette.lightBackground : palette.mediumBackground
            radius: 4
            anchors.horizontalCenter: menu.horizontalCenter
            anchors.bottom: menu.verticalCenter
            anchors.bottomMargin: 5

            MouseArea {
                anchors.centerIn: parent
                width: parent.width
                height: parent.height
                onClicked: {
                    menu.visible = false
                    view_tabbed.visible = true
                    GUI.start()
                }
                onPressed: {
                    start_tabbed.color = palette.lightBackground
                    start_tabbed_text.color = palette.darkBackground
                }
                onReleased: {
                    start_tabbed.color = palette.darkBackground
                    start_tabbed_text.color = GUI.connected ? palette.lightBackground : palette.mediumBackground
                }
                enabled: GUI.connected
            }

            Text {
                id: start_tabbed_text
                color: GUI.connected ? palette.lightBackground : palette.mediumBackground
                anchors.centerIn: parent
                text: "tabbed"
                font.capitalization: Font.AllUppercase
            }
        }

        Rectangle {
            id: start_graph
            width: 80
            height: 80
            color: palette.darkBackground
            border.width: 2
            border.color: GUI.connected ? palette.lightBackground : palette.mediumBackground
            radius: 4
            anchors.left: start_tabbed.right
            anchors.leftMargin: 10
            anchors.bottom: menu.verticalCenter
            anchors.bottomMargin: 5

            MouseArea {
                anchors.centerIn: parent
                width: parent.width
                height: parent.height
                onClicked: {
                    menu.visible = false
                    view_graph.visible = true
                    GUI.start()
                }
                onPressed: {
                    start_graph.color = palette.lightBackground
                    start_graph_text.color = palette.darkBackground
                }
                onReleased: {
                    start_graph.color = palette.darkBackground
                    start_graph_text.color = GUI.connected ? palette.lightBackground : palette.mediumBackground
                }
                enabled: GUI.connected
            }

            Text {
                id: start_graph_text
                color: GUI.connected ? palette.lightBackground : palette.mediumBackground
                anchors.centerIn: parent
                text: "graph"
                font.capitalization: Font.AllUppercase
            }
        }

        Rectangle {
            id: connect
            width: 80
            height: 80
            color: palette.darkBackground
            border.width: 2
            border.color: GUI.connected ? palette.mediumBackground : palette.lightBackground
            radius: 4
            anchors.top: menu.verticalCenter
            anchors.topMargin: 5
            anchors.right: menu.horizontalCenter
            anchors.rightMargin: 5

            MouseArea {
                anchors.centerIn: parent
                width: parent.width
                height: parent.height
                onClicked: {
                    GUI.connect()
                }
                onPressed: {
                    connect.color = palette.lightBackground
                    connect_text.color = GUI.connected ? palette.mediumBackground : palette.lightBackground
                }
                onReleased: {
                    connect.color = palette.darkBackground
                    connect_text.color = GUI.connected ? palette.mediumBackground : palette.lightBackground
                }
                enabled: !GUI.connected
            }

            Text {
                id: connect_text
                color: GUI.connected ? palette.mediumBackground : palette.lightBackground
                anchors.centerIn: parent
                text: "connect"
                font.capitalization: Font.AllUppercase
            }
        }

        Rectangle {
            id: exit
            width: 80
            height: 80
            color: palette.darkBackground
            border.width: 2
            border.color: palette.lightBackground
            radius: 4
            anchors.top: menu.verticalCenter
            anchors.topMargin: 5
            anchors.left: menu.horizontalCenter
            anchors.leftMargin: 5

            MouseArea {
                anchors.centerIn: parent
                width: parent.width
                height: parent.height
                onClicked: {
                    Qt.quit()
                }
                onPressed: {
                    exit.color = palette.lightBackground
                    exit_text.color = palette.darkBackground
                }
                onReleased: {
                    exit.color = palette.darkBackground
                    exit_text.color = palette.lightBackground
                }
            }

            Text {
                id: exit_text
                color: palette.lightBackground
                anchors.centerIn: parent
                text: "exit"
                font.capitalization: Font.AllUppercase
            }
        }
    }

    Rectangle {
        id: view_simple
        width: main_window.width
        height: main_window.height - topbar.height
        color: palette.lightBackground
        anchors.top: topbar.bottom
        anchors.horizontalCenter: topbar.horizontalCenter
        visible: false

        Rectangle {
            id: container1
            width: 82
            height:view_simple.height - 38
            border.width: 1
            border.color: "black"
            anchors.left: view_simple.left
            anchors.leftMargin: 19
            anchors.verticalCenter: view_simple.verticalCenter

            Rectangle {
                id: level1
                width: 80
                height: (GUI.c_level1 / constants.max_fill) * (view_simple.height - 40)
                anchors.bottom: container1.bottom
                anchors.bottomMargin: 1
                anchors.horizontalCenter: container1.horizontalCenter
                color: (GUI.c_sensor1 == 0) ? palette.neutral : palette.medium;
            }

            Text {
                text: Math.round(GUI.c_level1 * 100) / 100
                font.bold: true
                anchors.top: level1.top
                anchors.topMargin: (GUI.c_sensor1 == -1) ? -15 : 1;
                anchors.horizontalCenter: level1.horizontalCenter
                color: (GUI.c_sensor1 == -1) ? palette.darkBackground : palette.lightBackground;
            }
        }

        Rectangle {
            id: container2
            width: 82
            height:view_simple.height - 38
            border.width: 1
            border.color: "black"
            anchors.left: container1.right
            anchors.leftMargin: 18
            anchors.verticalCenter: view_simple.verticalCenter

            Rectangle {
                id: level2
                width: 80
                height: (GUI.c_level2 / constants.max_fill) * (view_simple.height - 40)
                anchors.bottom: container2.bottom
                anchors.bottomMargin: 1
                anchors.horizontalCenter: container2.horizontalCenter
                color: (GUI.c_sensor2 == 0) ? palette.neutral : palette.medium;
            }

            Text {
                text: Math.round(GUI.c_level2 * 100) / 100
                font.bold: true
                anchors.top: level2.top
                anchors.topMargin: (GUI.c_sensor2 == -1) ? -15 : 1;
                anchors.horizontalCenter: level2.horizontalCenter
                color: (GUI.c_sensor2 == -1) ? palette.darkBackground : palette.lightBackground;
            }
        }

        Rectangle {
            id: container3
            width: 82
            height:view_simple.height - 38
            border.width: 1
            border.color: "black"
            anchors.left: container2.right
            anchors.leftMargin: 18
            anchors.verticalCenter: view_simple.verticalCenter

            Rectangle {
                id: level3
                width: 80
                height: (GUI.c_level3 / constants.max_fill) * (view_simple.height - 40)
                anchors.bottom: container3.bottom
                anchors.bottomMargin: 1
                anchors.horizontalCenter: container3.horizontalCenter
                color: (GUI.c_sensor3 == 0) ? palette.neutral : palette.medium;
            }

            Text {
                text: Math.round(GUI.c_level3 * 100) / 100
                font.bold: true
                anchors.top: level3.top
                anchors.topMargin: (GUI.c_sensor3 == -1) ? -15 : 1;
                anchors.horizontalCenter: level3.horizontalCenter
                color: (GUI.c_sensor3 == -1) ? palette.darkBackground : palette.lightBackground;
            }
        }
    }

    Rectangle {
        id: view_tabbed
        width: main_window.width
        height: main_window.height - topbar.height
        color: palette.lightBackground
        anchors.top: topbar.bottom
        anchors.horizontalCenter: topbar.horizontalCenter
        visible: false

        Rectangle {
            id: detail_view1
            width: main_window.width
            height: main_window.height - topbar.height - tab_bar.height
            anchors.top: view_tabbed.top
            anchors.horizontalCenter: view_tabbed.horizontalCenter
            color:palette.lightBackground
            visible: true

            Rectangle {
                id: container_detail1
                height: parent.height - 36
                width: 44
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 20
                border.width: 2
                border.color: palette.darkBackground
                radius: 4
                color: palette.lightBackground

                Rectangle {
                    id: fill1
                    height: (GUI.c_level1 / constants.max_fill) * (parent.height - 4)
                    width: parent.width - 4
                    color: palette.neutral
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 2
                    anchors.horizontalCenter: parent.horizontalCenter

                }

                Rectangle {
                    id: sensor_top1
                    height: 20
                    width: 20
                    radius: 10
                    color: (GUI.c_sensor1 == 1) ? palette.medium : palette.good
                    border.color: palette.darkBackground
                    border.width: 1
                    anchors.left: container_detail1.right
                    anchors.leftMargin: 6
                    anchors.top: container_detail1.top
                }

                Rectangle {
                    id: sensor_bottom1
                    height: 20
                    width: 20
                    radius: 10
                    color: (GUI.c_sensor1 == -1) ? palette.medium : palette.good
                    border.color: palette.darkBackground
                    border.width: 1
                    anchors.left: container_detail1.right
                    anchors.leftMargin: 6
                    anchors.bottom: container_detail1.bottom
                }

                Rectangle {

                    width: 20
                    height: 20
                    anchors.left: container_detail1.right
                    anchors.leftMargin: 6
                    anchors.bottom: container_detail1.verticalCenter
                    anchors.bottomMargin: 5
                    color: "transparent"

                    Canvas {
                        id: flow_up1
                        width: 20
                        height: 20
                        antialiasing: true
                        anchors.centerIn: parent
                        property bool filled: GUI.target == 1 && GUI.transfer

                        onFilledChanged: {
                            requestPaint()
                        }

                        onPaint: {
                            var ctx = getContext("2d");
                            ctx.save();
                            ctx.clearRect(x, y, width, height);
                            ctx.lineJoin = "round";
                            ctx.strokeStyle = palette.darkBackground
                            ctx.lineWidth = 2
                            ctx.fillStyle  = palette.neutral
                            ctx.beginPath();
                            ctx.moveTo(x + (width / 2), y);
                            ctx.lineTo(x + width, y + height);
                            ctx.lineTo(x, y + height);
                            ctx.closePath();
                            ctx.stroke();
                            if(filled){
                                ctx.fill();
                            }
                            ctx.restore();
                        }
                    }

                }

                Rectangle {

                    width: 20
                    height: 20
                    anchors.left: container_detail1.right
                    anchors.leftMargin: 6
                    anchors.top: container_detail1.verticalCenter
                    anchors.topMargin: 5
                    color: "transparent"

                    Canvas {
                        id: flow_down1
                        width: 20
                        height: 20
                        antialiasing: true
                        anchors.centerIn: parent
                        property bool filled: GUI.source == 1 && GUI.transfer
                        onFilledChanged: {
                            requestPaint()
                        }

                        onPaint: {
                            var ctx = getContext("2d");
                            ctx.save();
                            ctx.clearRect(x, y, width, height);
                            ctx.lineJoin = "round";
                            ctx.strokeStyle = palette.darkBackground
                            ctx.lineWidth = 2
                            ctx.fillStyle  = palette.neutral
                            ctx.beginPath();
                            ctx.moveTo(x, y);
                            ctx.lineTo(x + width, y);
                            ctx.lineTo(x + width / 2, y + height);
                            ctx.closePath();
                            ctx.stroke();
                            if(filled){
                                ctx.fill();
                            }
                            ctx.restore();
                        }
                    }

                }
            }

            Rectangle {
                id: detail_text1
                width: parent.width / 2
                height: parent.height
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                color: palette.lightBackground

                Text {
                    id: detail_name1
                    width: parent.width
                    height: 20
                    anchors.top: parent.top
                    anchors.topMargin: 18
                    anchors.left: parent.left
                    text: "Container 1"
                    font.bold: true
                }

                Text {
                    id: detail_level1
                    width: parent.width
                    height: 20
                    anchors.top: detail_name1.bottom
                    anchors.left: parent.left
                    text: "Level: " + Math.round(GUI.c_level1 * 100)/100
                }

                Text {
                    id: detail_last_level1
                    width: parent.width
                    height: 20
                    anchors.top: detail_level1.bottom
                    anchors.left: parent.left
                    text: "Change: " + Math.round((GUI.c_level1 - GUI.c_last_level1) * 100) / 100
                }

                Text {
                    id: detail_transfer1
                    width: parent.width
                    height: 20
                    anchors.top: detail_last_level1.bottom
                    anchors.left: parent.left
                    text: "Transfer: " + (GUI.transfer ? "active" : "inactive")
                }

                Text {
                    id: detail_source1
                    width: parent.width
                    height: 20
                    anchors.top: detail_transfer1.bottom
                    anchors.left: parent.left
                    text: "Source: Container " + GUI.source
                }

                Text {
                    id: detail_target1
                    width: parent.width
                    height: 20
                    anchors.top: detail_source1.bottom
                    anchors.left: parent.left
                    text: "Target: Container " + GUI.target
                }
            }
        }

        Rectangle {
            id: detail_view2
            width: main_window.width
            height: main_window.height - topbar.height - tab_bar.height
            anchors.top: view_tabbed.top
            anchors.horizontalCenter: view_tabbed.horizontalCenter
            color:palette.lightBackground
            visible: false

            Rectangle {
                id: container_detail2
                height: parent.height - 36
                width: 44
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 20
                border.width: 2
                border.color: palette.darkBackground
                radius: 4
                color: palette.lightBackground

                Rectangle {
                    id: fill2
                    height: (GUI.c_level2 / constants.max_fill) * (parent.height - 4)
                    width: parent.width - 4
                    color: palette.neutral
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 2
                    anchors.horizontalCenter: parent.horizontalCenter

                }

                Rectangle {
                    id: sensor_top2
                    height: 20
                    width: 20
                    radius: 10
                    color: (GUI.c_sensor2 == 1) ? palette.medium : palette.good
                    border.color: palette.darkBackground
                    border.width: 1
                    anchors.left: container_detail2.right
                    anchors.leftMargin: 6
                    anchors.top: container_detail2.top
                }

                Rectangle {
                    id: sensor_bottom2
                    height: 20
                    width: 20
                    radius: 10
                    color: (GUI.c_sensor2 == -1) ? palette.medium : palette.good
                    border.color: palette.darkBackground
                    border.width: 1
                    anchors.left: container_detail2.right
                    anchors.leftMargin: 6
                    anchors.bottom: container_detail2.bottom
                }

                Rectangle {

                    width: 20
                    height: 20
                    anchors.left: container_detail2.right
                    anchors.leftMargin: 6
                    anchors.bottom: container_detail2.verticalCenter
                    anchors.bottomMargin: 5
                    color: "transparent"

                    Canvas {
                        id: flow_up2
                        width: 20
                        height: 20
                        antialiasing: true
                        anchors.centerIn: parent
                        property bool filled: GUI.target == 2 && GUI.transfer

                        onFilledChanged: {
                            requestPaint()
                        }

                        onPaint: {
                            var ctx = getContext("2d");
                            ctx.save();
                            ctx.clearRect(x, y, width, height);
                            ctx.lineJoin = "round";
                            ctx.strokeStyle = palette.darkBackground
                            ctx.lineWidth = 2
                            ctx.fillStyle  = palette.neutral
                            ctx.beginPath();
                            ctx.moveTo(x + (width / 2), y);
                            ctx.lineTo(x + width, y + height);
                            ctx.lineTo(x, y + height);
                            ctx.closePath();
                            ctx.stroke();
                            if(filled){
                                ctx.fill();
                            }
                            ctx.restore();
                        }
                    }

                }

                Rectangle {

                    width: 20
                    height: 20
                    anchors.left: container_detail2.right
                    anchors.leftMargin: 6
                    anchors.top: container_detail2.verticalCenter
                    anchors.topMargin: 5
                    color: "transparent"

                    Canvas {
                        id: flow_down2
                        width: 20
                        height: 20
                        antialiasing: true
                        anchors.centerIn: parent
                        property bool filled: GUI.source == 2 && GUI.transfer
                        onFilledChanged: {
                            requestPaint()
                        }

                        onPaint: {
                            var ctx = getContext("2d");
                            ctx.save();
                            ctx.clearRect(x, y, width, height);
                            ctx.lineJoin = "round";
                            ctx.strokeStyle = palette.darkBackground
                            ctx.lineWidth = 2
                            ctx.fillStyle  = palette.neutral
                            ctx.beginPath();
                            ctx.moveTo(x, y);
                            ctx.lineTo(x + width, y);
                            ctx.lineTo(x + width / 2, y + height);
                            ctx.closePath();
                            ctx.stroke();
                            if(filled){
                                ctx.fill();
                            }
                            ctx.restore();
                        }
                    }

                }
            }

            Rectangle {
                id: detail_text2
                width: parent.width / 2
                height: parent.height
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                color: palette.lightBackground

                Text {
                    id: detail_name2
                    width: parent.width
                    height: 20
                    anchors.top: parent.top
                    anchors.topMargin: 18
                    anchors.left: parent.left
                    text: "Container 2"
                    font.bold: true
                }

                Text {
                    id: detail_level2
                    width: parent.width
                    height: 20
                    anchors.top: detail_name2.bottom
                    anchors.left: parent.left
                    text: "Level: " + Math.round(GUI.c_level2 * 100)/100
                }

                Text {
                    id: detail_last_level2
                    width: parent.width
                    height: 20
                    anchors.top: detail_level2.bottom
                    anchors.left: parent.left
                    text: "Change: " + Math.round((GUI.c_level2 - GUI.c_last_level2) * 100) / 100
                }

                Text {
                    id: detail_transfer2
                    width: parent.width
                    height: 20
                    anchors.top: detail_last_level2.bottom
                    anchors.left: parent.left
                    text: "Transfer: " + (GUI.transfer ? "active" : "inactive")
                }

                Text {
                    id: detail_source2
                    width: parent.width
                    height: 20
                    anchors.top: detail_transfer2.bottom
                    anchors.left: parent.left
                    text: "Source: Container " + GUI.source
                }

                Text {
                    id: detail_target2
                    width: parent.width
                    height: 20
                    anchors.top: detail_source2.bottom
                    anchors.left: parent.left
                    text: "Target: Container " + GUI.target
                }
            }
        }

        Rectangle {
            id: detail_view3
            width: main_window.width
            height: main_window.height - topbar.height - tab_bar.height
            anchors.top: view_tabbed.top
            anchors.horizontalCenter: view_tabbed.horizontalCenter
            color:palette.lightBackground
            visible: false

            Rectangle {
                id: container_detail3
                height: parent.height - 36
                width: 44
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 20
                border.width: 2
                border.color: palette.darkBackground
                radius: 4
                color: palette.lightBackground

                Rectangle {
                    id: fill
                    height: (GUI.c_level3 / constants.max_fill) * (parent.height - 4)
                    width: parent.width - 4
                    color: palette.neutral
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 2
                    anchors.horizontalCenter: parent.horizontalCenter

                }

                Rectangle {
                    id: sensor_top3
                    height: 20
                    width: 20
                    radius: 10
                    color: (GUI.c_sensor3 == 1) ? palette.medium : palette.good
                    border.color: palette.darkBackground
                    border.width: 1
                    anchors.left: container_detail3.right
                    anchors.leftMargin: 6
                    anchors.top: container_detail3.top
                }

                Rectangle {
                    id: sensor_bottom3
                    height: 20
                    width: 20
                    radius: 10
                    color: (GUI.c_sensor3 == -1) ? palette.medium : palette.good
                    border.color: palette.darkBackground
                    border.width: 1
                    anchors.left: container_detail3.right
                    anchors.leftMargin: 6
                    anchors.bottom: container_detail3.bottom
                }

                Rectangle {

                    width: 20
                    height: 20
                    anchors.left: container_detail3.right
                    anchors.leftMargin: 6
                    anchors.bottom: container_detail3.verticalCenter
                    anchors.bottomMargin: 5
                    color: "transparent"

                    Canvas {
                        id: flow_up3
                        width: 20
                        height: 20
                        antialiasing: true
                        anchors.centerIn: parent
                        property bool filled: GUI.target == 3 && GUI.transfer

                        onFilledChanged: {
                            requestPaint()
                        }

                        onPaint: {
                            var ctx = getContext("2d");
                            ctx.save();
                            ctx.clearRect(x, y, width, height);
                            ctx.lineJoin = "round";
                            ctx.strokeStyle = palette.darkBackground
                            ctx.lineWidth = 2
                            ctx.fillStyle  = palette.neutral
                            ctx.beginPath();
                            ctx.moveTo(x + (width / 2), y);
                            ctx.lineTo(x + width, y + height);
                            ctx.lineTo(x, y + height);
                            ctx.closePath();
                            ctx.stroke();
                            if(filled){
                                ctx.fill();
                            }
                            ctx.restore();
                        }
                    }

                }

                Rectangle {

                    width: 20
                    height: 20
                    anchors.left: container_detail3.right
                    anchors.leftMargin: 6
                    anchors.top: container_detail3.verticalCenter
                    anchors.topMargin: 5
                    color: "transparent"

                    Canvas {
                        id: flow_down3
                        width: 20
                        height: 20
                        antialiasing: true
                        anchors.centerIn: parent
                        property bool filled: GUI.source == 3 && GUI.transfer
                        onFilledChanged: {
                            requestPaint()
                        }

                        onPaint: {
                            var ctx = getContext("2d");
                            ctx.save();
                            ctx.clearRect(x, y, width, height);
                            ctx.lineJoin = "round";
                            ctx.strokeStyle = palette.darkBackground
                            ctx.lineWidth = 2
                            ctx.fillStyle  = palette.neutral
                            ctx.beginPath();
                            ctx.moveTo(x, y);
                            ctx.lineTo(x + width, y);
                            ctx.lineTo(x + width / 2, y + height);
                            ctx.closePath();
                            ctx.stroke();
                            if(filled){
                                ctx.fill();
                            }
                            ctx.restore();
                        }
                    }

                }
            }

            Rectangle {
                id: detail_text3
                width: parent.width / 2
                height: parent.height
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                color: palette.lightBackground

                Text {
                    id: detail_name3
                    width: parent.width
                    height: 20
                    anchors.top: parent.top
                    anchors.topMargin: 18
                    anchors.left: parent.left
                    text: "Container 3"
                    font.bold: true
                }

                Text {
                    id: detail_level3
                    width: parent.width
                    height: 20
                    anchors.top: detail_name3.bottom
                    anchors.left: parent.left
                    text: "Level: " + Math.round(GUI.c_level3 * 100)/100
                }

                Text {
                    id: detail_last_level3
                    width: parent.width
                    height: 20
                    anchors.top: detail_level3.bottom
                    anchors.left: parent.left
                    text: "Change: " + Math.round((GUI.c_level3 - GUI.c_last_level3) * 100) / 100
                }

                Text {
                    id: detail_transfer3
                    width: parent.width
                    height: 20
                    anchors.top: detail_last_level3.bottom
                    anchors.left: parent.left
                    text: "Transfer: " + (GUI.transfer ? "active" : "inactive")
                }

                Text {
                    id: detail_source3
                    width: parent.width
                    height: 20
                    anchors.top: detail_transfer3.bottom
                    anchors.left: parent.left
                    text: "Source: Container " + GUI.source
                }

                Text {
                    id: detail_target3
                    width: parent.width
                    height: 20
                    anchors.top: detail_source3.bottom
                    anchors.left: parent.left
                    text: "Target: Container " + GUI.target
                }
            }
        }

        Rectangle {
            id: tab_bar
            height: 44
            width: main_window.width
            anchors.bottom: view_tabbed.bottom
            anchors.horizontalCenter: view_tabbed.horizontalCenter
            color: palette.darkBackground

            Rectangle {
                id: tab1
                width: 80
                height: 40
                anchors.left: tab_bar.left
                anchors.leftMargin: 20
                anchors.verticalCenter: tab_bar.verticalCenter
                color:palette.darkBackground
                border.width: 2
                border.color: palette.neutral
                radius: 4

                MouseArea {
                    width: parent.width
                    height: parent.height
                    anchors.centerIn: parent

                    onPressed: {
                        parent.color = palette.lightBackground
                        parent.border.color = palette.darkBackground
                    }

                    onReleased: {
                        parent.color = palette.darkBackground
                        parent.border.color = palette.lightBackground
                    }

                    onClicked: {
                        parent.border.color = palette.neutral
                        tab2.border.color = palette.lightBackground
                        tab3.border.color = palette.lightBackground

                        detail_view1.visible = true
                        detail_view2.visible = false
                        detail_view3.visible = false
                    }
                }

                Rectangle {
                    id: tab_text1
                    width: parent.width / 2 - 5
                    height: parent.height - 4
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 5
                    color: parent.color

                    Text {
                        anchors.centerIn: parent
                        color: parent.parent.border.color
                        text: "C1"
                    }
                }

                Rectangle {
                    id: tab_preview1
                    width: tab_text1.width
                    height: tab_text1.height
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    anchors.rightMargin: 5
                    color: parent.color

                    Rectangle {
                        width: 10
                        height: parent.height - 6
                        color: palette.lightBackground
                        border.color: parent.parent.border.color
                        border.width: 1
                        radius: 1
                        anchors.centerIn: parent

                        Rectangle {
                            width: parent.width - 2
                            height: (GUI.c_level1 / constants.max_fill) * (parent.height - 2)
                            color: (GUI.c_sensor1 == 0) ? palette.neutral : palette.medium;
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: 1
                        }
                    }
                }
            }

            Rectangle {
                id: tab2
                width: 80
                height: 40
                anchors.left: tab1.right
                anchors.leftMargin: 20
                anchors.verticalCenter: tab_bar.verticalCenter
                color:palette.darkBackground
                border.width: 2
                border.color: palette.lightBackground
                radius: 4

                MouseArea {
                    width: parent.width
                    height: parent.height
                    anchors.centerIn: parent

                    onPressed: {
                        parent.color = palette.lightBackground
                        parent.border.color = palette.darkBackground
                    }

                    onReleased: {
                        parent.color = palette.darkBackground
                        parent.border.color = palette.lightBackground
                    }

                    onClicked: {
                        parent.border.color = palette.neutral
                        tab1.border.color = palette.lightBackground
                        tab3.border.color = palette.lightBackground

                        detail_view1.visible = false
                        detail_view2.visible = true
                        detail_view3.visible = false
                    }
                }

                Rectangle {
                    id: tab_text2
                    width: parent.width / 2 - 5
                    height: parent.height - 4
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 5
                    color: parent.color

                    Text {
                        anchors.centerIn: parent
                        color: parent.parent.border.color
                        text: "C2"
                    }
                }

                Rectangle {
                    id: tab_preview2
                    width: tab_text2.width
                    height: tab_text2.height
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    anchors.rightMargin: 5
                    color: parent.color

                    Rectangle {
                        width: 10
                        height: parent.height - 6
                        color: palette.lightBackground
                        border.color: parent.parent.border.color
                        border.width: 1
                        radius: 1
                        anchors.centerIn: parent

                        Rectangle {
                            width: parent.width - 2
                            height: (GUI.c_level2 / constants.max_fill) * (parent.height - 2)
                            color: (GUI.c_sensor2 == 0) ? palette.neutral : palette.medium;
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: 1
                        }
                    }
                }
            }

            Rectangle {
                id: tab3
                width: 80
                height: 40
                anchors.left: tab2.right
                anchors.leftMargin: 20
                anchors.verticalCenter: tab_bar.verticalCenter
                color:palette.darkBackground
                border.width: 2
                border.color: palette.lightBackground
                radius: 4

                MouseArea {
                    width: parent.width
                    height: parent.height
                    anchors.centerIn: parent

                    onPressed: {
                        parent.color = palette.lightBackground
                        parent.border.color = palette.darkBackground
                    }

                    onReleased: {
                        parent.color = palette.darkBackground
                        parent.border.color = palette.lightBackground
                    }

                    onClicked: {
                        parent.border.color = palette.neutral
                        tab1.border.color = palette.lightBackground
                        tab2.border.color = palette.lightBackground

                        detail_view1.visible = false
                        detail_view2.visible = false
                        detail_view3.visible = true
                    }
                }

                Rectangle {
                    id: tab_text3
                    width: parent.width / 2 - 5
                    height: parent.height - 4
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 5
                    color: parent.color

                    Text {
                        anchors.centerIn: parent
                        color: parent.parent.border.color
                        text: "C3"
                    }
                }

                Rectangle {
                    id: tab_preview3
                    width: tab_text3.width
                    height: tab_text3.height
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    anchors.rightMargin: 5
                    color: parent.color

                    Rectangle {
                        width: 10
                        height: parent.height - 6
                        color: palette.lightBackground
                        border.color: parent.parent.border.color
                        border.width: 1
                        radius: 1
                        anchors.centerIn: parent

                        Rectangle {
                            width: parent.width - 2
                            height: (GUI.c_level3 / constants.max_fill) * (parent.height - 2)
                            color: (GUI.c_sensor3 == 0) ? palette.neutral : palette.medium;
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: 1
                        }
                    }
                }
            }
        }
    }

    Rectangle {
        id: view_graph
        width: main_window.width
        height: main_window.height - topbar.height
        color: palette.lightBackground
        anchors.top: topbar.bottom
        anchors.horizontalCenter: topbar.horizontalCenter
        visible: false

        Rectangle {
            id: graph_container1
            width: (parent.width - 20) / 10
            height: (parent.height - 20) / 2
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.top: parent.top
            anchors.topMargin: 10
            color: palette.lightBackground
            border.width: 1
            border.color: palette.darkBackground

            Rectangle {
                width: parent.width - 2
                height: (GUI.c_level1 / constants.max_fill) * (parent.height - 2)
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 1
                anchors.horizontalCenter: parent.horizontalCenter
                color: (GUI.c_sensor1 == 0) ? palette.neutral : palette.medium;
            }
        }

        Rectangle {
            id: graph_container2
            width: (parent.width - 20) / 10
            height: (parent.height - 20) / 2
            anchors.left: graph_container1.right
            anchors.leftMargin: 50
            anchors.top: graph_container1.top
            color: palette.lightBackground
            border.width: 1
            border.color: palette.darkBackground

            Rectangle {
                width: parent.width - 2
                height: (GUI.c_level2 / constants.max_fill) * (parent.height - 2)
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 1
                anchors.horizontalCenter: parent.horizontalCenter
                color: (GUI.c_sensor2 == 0) ? palette.neutral : palette.medium;
            }
        }

        Rectangle {
            id: graph_container3
            width: (parent.width - 20) / 10
            height: (parent.height - 20) / 2
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            color: palette.lightBackground
            border.width: 1
            border.color: palette.darkBackground

            Rectangle {
                width: parent.width - 2
                height: (GUI.c_level3 / constants.max_fill) * (parent.height - 2)
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 1
                anchors.horizontalCenter: parent.horizontalCenter
                color: (GUI.c_sensor3 == 0) ? palette.neutral : palette.medium;
            }
        }

        Rectangle {
            id: graph_text
            height: graph_container3.y - parent.y
            width: parent.width / 2
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.topMargin: 10
            color: palette.lightBackground

            Text {
                id: container_text1
                anchors.bottom: container_text2.top
                anchors.left: container_text2.left
                anchors.bottomMargin: 3
                text: "Container 1: " + Math.round(GUI.c_level1 * 100)/100
            }

            Text {
                id: container_text2
                anchors.centerIn: parent
                text: "Container 2: " + Math.round(GUI.c_level2 * 100)/100
            }

            Text{
                id: container_text3
                anchors.top: container_text2.bottom
                anchors.left: container_text2.left
                anchors.topMargin: 3
                text: "Container 3: " + Math.round(GUI.c_level3 * 100)/100
            }
        }

        Rectangle {
            id: pipe1
            width: 20
            height: constants.pipe_size
            anchors.left: graph_container1.right
            anchors.top: graph_container1.top
            anchors.topMargin: 5
            color: palette.mediumBackground
        }

        Rectangle {
            id: pipe2
            width: constants.pipe_size
            height: pipe3.y - pipe1.y
            anchors.top: pipe1.bottom
            anchors.right: pipe1.right
            color: palette.mediumBackground
        }

        Rectangle {
            id: pipe3
            width: graph_container3.x - (pipe2.x + pipe2.width)
            height: constants.pipe_size
            anchors.right: graph_container3.left
            anchors.bottom: graph_container3.bottom
            anchors.bottomMargin: 5
            color: palette.mediumBackground
        }

        Rectangle {
            id: pipe4
            width: constants.pipe_size
            height: (pipe5.y + pipe5.height) - (graph_container2.y + graph_container2.height)
            anchors.top: graph_container2.bottom
            anchors.horizontalCenter: graph_container2.horizontalCenter
            color: palette.mediumBackground
        }

        Rectangle {
            id: pipe5
            width: graph_container3.x - (pipe4.x + pipe4.width)
            height: constants.pipe_size
            anchors.right: graph_container3.left
            anchors.top: graph_container3.top
            anchors.topMargin: 15
            color: palette.mediumBackground
        }

        Rectangle {
            id: pipe6
            width: constants.pipe_size
            height: (pipe7.y +pipe7.height) - (graph_container1.y + graph_container1.height)
            anchors.top: graph_container1.bottom
            anchors.horizontalCenter: graph_container1.horizontalCenter
            color: palette.mediumBackground
        }

        Rectangle {
            id: pipe7
            width: graph_container3.x - (pipe6.x + pipe6.width)
            height: constants.pipe_size
            anchors.right: graph_container3.left
            anchors.top: pipe5.bottom
            anchors.topMargin: 10
            color: palette.mediumBackground
        }

        Rectangle {
            id: pipe8
            width: 20
            height: constants.pipe_size
            anchors.left: graph_container2.right
            anchors.top: graph_container2.top
            anchors.topMargin: 5
            color: palette.mediumBackground
        }

        Rectangle {
            id: pipe9
            width: constants.pipe_size
            height: pipe3.y - (pipe8.y + pipe8.height)
            anchors.top: pipe8.bottom
            anchors.right: pipe8.right
            color: palette.mediumBackground
        }

        Rectangle {
            id: pipe10
            width: constants.pipe_size
            height: pipe3.y - (pipe7.y + pipe7.height)
            anchors.bottom: pipe3.top
            anchors.right: graph_container3.left
            anchors.rightMargin: 25
            color: palette.mediumBackground
        }

        Rectangle {
            id: pipe11
            width: constants.pipe_size
            height: pipe3.y - (pipe5.y + pipe5.height)
            anchors.bottom: pipe3.top
            anchors.right: pipe10.left
            anchors.rightMargin: 35
            color: palette.mediumBackground
        }

        Rectangle {
            id: valve1
            width: constants.valve_size
            height: constants.valve_size
            radius: constants.valve_size
            z: 2
            border.width: 1
            border.color: palette.darkBackground
            anchors.centerIn: pipe1
            color: GUI.valve1 ? palette.neutral : palette.lightBackground
        }

        Rectangle {
            id: valve2
            width: constants.valve_size
            height: constants.valve_size
            radius: constants.valve_size
            z: 2
            border.width: 1
            border.color: palette.darkBackground
            anchors.centerIn: pipe8
            color: GUI.valve2 ? palette.neutral : palette.lightBackground
        }

        Rectangle {
            id: valve3
            width: constants.valve_size
            height: constants.valve_size
            radius: constants.valve_size
            border.width: 1
            border.color: palette.darkBackground
            anchors.centerIn: pipe11
            color: GUI.valve3 ? palette.neutral : palette.lightBackground
        }

        Rectangle {
            id: valve4
            width: constants.valve_size
            height: constants.valve_size
            radius: constants.valve_size
            border.width: 1
            border.color: palette.darkBackground
            anchors.horizontalCenter: pipe10.horizontalCenter
            anchors.verticalCenter: valve3.verticalCenter
            color: GUI.valve4 ? palette.neutral : palette.lightBackground
        }

        Rectangle {
            id: valve5
            width: constants.valve_size
            height: constants.valve_size
            radius: constants.valve_size
            z: 2
            border.width: 1
            border.color: palette.darkBackground
            anchors.verticalCenter: pipe5.verticalCenter
            anchors.right: graph_container3.left
            anchors.rightMargin: 5
            color: GUI.valve5 ? palette.neutral : palette.lightBackground
        }

        Rectangle {
            id: valve6
            width: constants.valve_size
            height: constants.valve_size
            radius: constants.valve_size
            z: 2
            border.width: 1
            border.color: palette.darkBackground
            anchors.verticalCenter: pipe7.verticalCenter
            anchors.right: graph_container3.left
            anchors.rightMargin: 5
            color: GUI.valve6 ? palette.neutral : palette.lightBackground
        }

        Rectangle {
            id: valve7
            width: constants.valve_size
            height: constants.valve_size
            radius: constants.valve_size
            z: 2
            border.width: 1
            border.color: palette.darkBackground
            anchors.verticalCenter: pipe3.verticalCenter
            anchors.right: graph_container3.left
            anchors.rightMargin: 5
            color: GUI.valve7 ? palette.neutral : palette.lightBackground
        }

        Rectangle {
            id: valve8
            width: constants.valve_size
            height: constants.valve_size
            radius: constants.valve_size
            z: 2
            border.width: 1
            border.color: palette.darkBackground
            anchors.verticalCenter: pipe7.verticalCenter
            anchors.left: pipe6.right
            anchors.leftMargin: 40
            color: GUI.valve8 ? palette.neutral : palette.lightBackground
        }

        Rectangle {
            id: valve9
            width: constants.valve_size
            height: constants.valve_size
            radius: constants.valve_size
            z: 2
            border.width: 1
            border.color: palette.darkBackground
            anchors.verticalCenter: pipe5.verticalCenter
            anchors.left: pipe4.right
            anchors.leftMargin: 40
            color: GUI.valve9 ? palette.neutral : palette.lightBackground
        }

        Rectangle {
            id: pump1
            width: constants.pump_size
            height: constants.pump_size
            border.width: 1
            border.color: palette.darkBackground
            anchors.verticalCenter: pipe7.verticalCenter
            anchors.left: pipe6.right
            anchors.leftMargin: 10
            color: GUI.pump1 ? palette.neutral : palette.lightBackground
        }

        Rectangle {
            id: pump2
            width: constants.pump_size
            height: constants.pump_size
            border.width: 1
            border.color: palette.darkBackground
            anchors.verticalCenter: pipe5.verticalCenter
            anchors.left: pipe4.left
            anchors.leftMargin: 10
            color: GUI.pump2 ? palette.neutral : palette.lightBackground
        }

        Rectangle {
            id: pump3
            width: constants.pump_size
            height: constants.pump_size
            border.width: 1
            border.color: palette.darkBackground
            anchors.verticalCenter: pipe3.verticalCenter
            anchors.left: pipe11.right
            anchors.leftMargin: 10
            color: GUI.pump3 ? palette.neutral : palette.lightBackground
        }

        Rectangle {
            width: constants.pipe_conn_size
            height: constants.pipe_conn_size
            radius: 4
            color: palette.mediumBackground
            anchors.horizontalCenter: pipe11.horizontalCenter
            anchors.verticalCenter: pipe3.verticalCenter
        }

        Rectangle {
            width: constants.pipe_conn_size
            height: constants.pipe_conn_size
            radius: 4
            color: palette.mediumBackground
            anchors.horizontalCenter: pipe10.horizontalCenter
            anchors.verticalCenter: pipe3.verticalCenter
        }

        Rectangle {
            width: constants.pipe_conn_size
            height: constants.pipe_conn_size
            radius: 4
            color: palette.mediumBackground
            anchors.horizontalCenter: pipe11.horizontalCenter
            anchors.verticalCenter: pipe5.verticalCenter
        }

        Rectangle {
            width: constants.pipe_conn_size
            height: constants.pipe_conn_size
            radius: 4
            color: palette.mediumBackground
            anchors.horizontalCenter: pipe10.horizontalCenter
            anchors.verticalCenter: pipe7.verticalCenter
        }

        Rectangle {
            width: constants.pipe_conn_size
            height: constants.pipe_conn_size
            radius: 4
            color: palette.mediumBackground
            anchors.horizontalCenter: pipe9.horizontalCenter
            anchors.verticalCenter: pipe3.verticalCenter
        }
    }
}
