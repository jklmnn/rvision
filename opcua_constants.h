/** @file */

#ifndef OPCUA_CONSTANTS_H
#define OPCUA_CONSTANTS_H

/**
 * @addtogroup OPCUA
 * @{
 */

#define XMLDASERVER     "1:XML DA Server - eats11"
#define SCHNEIDER       "8:Schneider"
#define FUELLSTAND      "8:Fuellstand"
#define SENS_HIGH       "8:LH"
#define SENS_LOW        "8:LL"
#define PUMP            "8:P"
#define IST             "_Ist"
#define SOLL            "_Soll"
#define TRANS_SRC       "8:Behaelter_A_FL"
#define TRANS_TGT       "8:Behaelter_B_FL"
#define TRANSFER        "8:Start_Umpumpen_FL"
#define VALVE_MAG       "8:Y"
#define VALVE_VEN       "8:V"

/** @} */

#endif // OPCUA_CONSTANTS_H
