/** @file */

#ifndef OPCUA_H
#define OPCUA_H

/*#ifndef __has_include
  #include <python3.4/Python.h>
  #define PYTHON3_VERSION 4
#else
  #if __has_include("python3.5/Python.h")
    #include <python3.5/Python.h>
    #define PYTHON3_VERSION 5
  #elif __has_include("python3.4/Python.h")
    #include <python3.4/Python.h>
    #define PYTHON3_VERSION 4
  #else
    #error Python >= 3.4 is required.
  #endif
#endif*/

#include "rvision.h"
#include <Python.h>
#include <string>
#include <logging.h>
#include "opcua_constants.h"
#include "machine.h"

class RVision;

/**
 * @defgroup OPCUA
 *
 * Dieses Modul ist für die Anbindung an den OPC UA Server verantwortlich.
 * Die Teile der OPC UA Namespaces und Identifier sind als Makros definiert und müssen im Falle einer Änderung auf dem Server entsprechend angepasst werden.
 *
 * @{
 */

/**
 *
 *
 * @brief Klasse des OPC UA CLients.
 */
class OPCUA
{
public:
    OPCUA(std::string, LogLevel);
    void connect(RVision*);
    int update(Machine*, RVision*);
    ~OPCUA();
private:
    std::string m_url;
    Logging *m_logger;
    PyObject *m_p_client;
    PyObject *m_p_data_node;
    bool m_connected;
    int p_call_opc(PyObject *parent, const std::string func, const std::string args[], const size_t size, PyObject **result);
    int get_value(std::string, PyObject**, RVision*);
    int get_sensor(std::string, int, RVision*, bool*);
    int get_level(int, RVision*, double*);
    int get_container(std::string, RVision*, int*);
    int get_active(std::string, RVision*, bool*);
};

/** @} */

#endif // OPCUA_H
