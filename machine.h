/** @file */

#ifndef MACHINE_H
#define MACHINE_H

/**
 * @defgroup Machine
 * @{
 */

/**
 *
 *
 * @brief Beinhaltet Füllstand und Sensorwerte eines Behälters.
 */
struct container_t {
    double level;
    bool upper;
    bool lower;
};

/**
 *
 *
 * @brief Beinhaltet Quelle, Ziel und Aktivität des Umpumpens.
 */
struct transfer_t {
    int source;
    int target;
    bool active;
};


/**
 *
 *
 * @brief Beinhaltet Werte der Ventile.
 */
struct valve_t {
    bool state[9];
};

/**
 *
 *
 * @brief Beinhaltet Werte der Pumpen.
 */
struct pump_t {
    bool state[3];
};

class Machine
{
public:
    Machine();
    container_t* get_container(int id);
    transfer_t* get_transfer();
    valve_t* get_valve();
    pump_t* get_pump();
private:
    container_t m_containers[3];
    transfer_t m_transfer;
    valve_t m_valves;
    pump_t m_pumps;
};

/** @} */

#endif // MACHINE_H
