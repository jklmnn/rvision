/** @file */

#include "gui.h"

/**
 * @addtogroup GUI
 * @{
 */

/**
 * Initialisiert die GUI mit sinnvollen Standardwerten.
 * Startet den Clockthread für Animationen.
 *
 * @brief GUI::GUI
 * @param parent
 */
GUI::GUI(QObject *parent) : QObject(parent)
{
    m_connected = false;
    m_fullscreen = false;
    m_working = false;
    m_rv = 0;

    m_clock = 0;
    m_clock_active = true;
    m_clock_t = std::thread(GUI::animationClock, this);

    m_c_level1 = 0;
    m_c_level2 = 0;
    m_c_level3 = 0;

    m_source = 1;
    m_target = 2;
    m_transfer = false;

    m_valve1 = false;
    m_valve2 = false;
    m_valve3 = false;
    m_valve4 = false;
    m_valve5 = false;
    m_valve6 = false;
    m_valve7 = false;
    m_valve8 = false;
    m_valve9 = false;

    m_pump1 = false;
    m_pump2 = false;
    m_pump3 = false;
}

/**
 * Beendet den Clockthread für Animationen.
 *
 * @brief GUI::~GUI
 */
GUI::~GUI()
{
    m_clock_active = false;
    m_clock_t.join();
}

/**
 * Zählt einen Counter zur Berechnung von Animationen.
 *
 * @brief GUI::animationClock
 * @param self Instanz der GUI.
 */
void GUI::animationClock(GUI *self)
{
    while(self->m_clock_active){
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
        self->setClock((self->clock() + 1) % 50);
    }
}

void GUI::setBackend(RVision *rv)
{
    m_rv = rv;
}

bool GUI::connected()
{
    return m_connected;
}

void GUI::setConnected(bool state)
{
    m_connected = state;
    Q_EMIT connectedChanged();
}

bool GUI::working()
{
    return m_working;
}

void GUI::setWorking(bool working)
{
    m_working = working;
    Q_EMIT workingChanged();
}

bool GUI::fullscreen()
{
    return m_fullscreen;
}

void GUI::setFullscreen(bool fullscreen)
{
    m_fullscreen = fullscreen;
    Q_EMIT fullscreenChanged();
}

/**
* Startet die Aktualisierung der Daten.
*
* @brief GUI::start
*/
void GUI::start()
{
    if (m_rv)
        m_rv->start();
}

/**
 * Stoppt die Aktualisierung der Daten.
 *
 * @brief GUI::stop
 */
void GUI::stop()
{
    if (m_rv)
        m_rv->pause();
}

/**
 * Startet einen Verbindungsversuch.
 *
 * @brief GUI::connect
 */
void GUI::connect()
{
    if (m_rv)
        m_rv->connect();
}

double GUI::c_level1()
{
    return m_c_level1;
}

void GUI::setC_level1(double c_level1)
{
    setC_last_level1(m_c_level1);
    m_c_level1 = c_level1;
    Q_EMIT c_level1Changed();
}

double GUI::c_level2()
{
    return m_c_level2;
}

void GUI::setC_level2(double c_level2)
{
    setC_last_level2(m_c_level2);
    m_c_level2 = c_level2;
    Q_EMIT c_level2Changed();
}

double GUI::c_level3()
{
    return m_c_level3;
}

void GUI::setC_level3(double c_level3)
{
    setC_last_level3(m_c_level3);
    m_c_level3 = c_level3;
    Q_EMIT c_level3Changed();
}

int GUI::c_sensor1()
{
    return m_c_sensor1;
}

void GUI::setC_sensor1(int c_sensor1)
{
    m_c_sensor1 = c_sensor1;
    Q_EMIT c_sensor1Changed();
}

int GUI::c_sensor2()
{
    return m_c_sensor2;
}

void GUI::setC_sensor2(int c_sensor2)
{
    m_c_sensor2 = c_sensor2;
    Q_EMIT c_sensor2Changed();
}

int GUI::c_sensor3()
{
    return m_c_sensor3;
}

void GUI::setC_sensor3(int c_sensor3)
{
    m_c_sensor3 = c_sensor3;
    Q_EMIT c_sensor3Changed();
}

QString GUI::name()
{
    return m_name;
}

void GUI::setName(QString name)
{
    m_name = name;
    Q_EMIT nameChanged();
}

QString GUI::version()
{
    return m_version;
}

void GUI::setVersion(QString version)
{
    m_version = version;
    Q_EMIT versionChanged();
}

int GUI::clock()
{
    return m_clock;
}

void GUI::setClock(int clock)
{
    m_clock = clock;
    Q_EMIT clockChanged();
}

bool GUI::valve1()
{
    return m_valve1;
}

void GUI::setValve1(bool valve1)
{
    m_valve1 = valve1;
    Q_EMIT valve1Changed();
}

bool GUI::valve2()
{
    return m_valve2;
}

void GUI::setValve2(bool valve2)
{
    m_valve2 = valve2;
    Q_EMIT valve2Changed();
}

bool GUI::valve3()
{
    return m_valve3;
}

void GUI::setValve3(bool valve3)
{
    m_valve3 = valve3;
    Q_EMIT valve3Changed();
}

bool GUI::valve4()
{
    return m_valve4;
}

void GUI::setValve4(bool valve4)
{
    m_valve4 = valve4;
    Q_EMIT valve4Changed();
}

bool GUI::valve5()
{
    return m_valve5;
}

void GUI::setValve5(bool valve5)
{
    m_valve5 = valve5;
    Q_EMIT valve5Changed();
}

bool GUI::valve6()
{
    return m_valve6;
}

void GUI::setValve6(bool valve6)
{
    m_valve6 = valve6;
    Q_EMIT valve6Changed();
}

bool GUI::valve7()
{
    return m_valve7;
}

void GUI::setValve7(bool valve7)
{
    m_valve7 = valve7;
    Q_EMIT valve7Changed();
}

bool GUI::valve8()
{
    return m_valve8;
}

void GUI::setValve8(bool valve8)
{
    m_valve8 = valve8;
    Q_EMIT valve8Changed();
}

bool GUI::valve9()
{
    return m_valve9;
}

void GUI::setValve9(bool valve9)
{
    m_valve9 = valve9;
    Q_EMIT valve9Changed();
}

bool GUI::pump1()
{
    return m_pump1;
}

void GUI::setPump1(bool pump1)
{
    m_pump1 = pump1;
    Q_EMIT pump1Changed();
}

bool GUI::pump2()
{
    return m_pump2;
}

void GUI::setPump2(bool pump2)
{
    m_pump2 = pump2;
    Q_EMIT pump2Changed();
}

bool GUI::pump3()
{
    return m_pump3;
}

void GUI::setPump3(bool pump3)
{
    m_pump3 = pump3;
    Q_EMIT pump3Changed();
}

double GUI::c_last_level1()
{
    return m_c_last_level1;
}

void GUI::setC_last_level1(double c_last_level1)
{
    m_c_last_level1 = c_last_level1;
    Q_EMIT c_last_level1Changed();
}

double GUI::c_last_level2()
{
    return m_c_last_level2;
}

void GUI::setC_last_level2(double c_last_level2)
{
    m_c_last_level2 = c_last_level2;
    Q_EMIT c_last_level2Changed();
}

double GUI::c_last_level3()
{
    return m_c_last_level3;
}

void GUI::setC_last_level3(double c_last_level3)
{
    m_c_last_level3 = c_last_level3;
    Q_EMIT c_last_level3Changed();
}

int GUI::source()
{
    return m_source;
}

void GUI::setSource(int source)
{
    m_source = source;
    Q_EMIT sourceChanged();
}

int GUI::target()
{
    return m_target;
}

void GUI::setTarget(int target)
{
    m_target = target;
    Q_EMIT targetChanged();
}

bool GUI::transfer()
{
    return m_transfer;
}

void GUI::setTransfer(bool transfer)
{
    m_transfer = transfer;
    Q_EMIT transferChanged();
}

/** @} */
